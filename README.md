# EEG Cognitive States Classifier 

The purpose of this project is to use EEG brain wave data to train a classifier to predict to one of two states. 

## Introduction
Please review the ```Homework-1.pdf``` file for a more in depth explanation of this project and it's goals. 

The overall purpose of this project is to predict brain states either "drowsy" or "alert" based on EEG data. We are given the raw EEG data and need to extract features to use for our classifier. For this experiement we are interested in the alpha and theta bands. There are also some intermediate files available as well. Please see below for complete descriptions below. 

## File Description
- ```EEG.yml``` used to setup your Anaconda envoirnment
- ```EEG_driving_data_sample.mat``` Matlab file of the original given data
- ```EEG-Notebook.ipynb``` Jupyter notebook file
- ```extract_channels_charles_version_FINAL.m``` Matlab file for the band extraction to export as features. This uses the new          ```bandpower``` function. 
- ```extracted_features.mat`` The extracted features used to train the classifier

## Setup
You will need to first setup your conda envoirnment. You can do this by using the command ```conda env create --file EEG.yml```. You will need Matlab in order to work with the ```.m``` files. A side note is that Octave does not currently support the ```bandpower``` function so you will need a recent version of Matlab in order to use the extraction features if you wish to try this part. Luckily, you can experiment with the data and training classifiers etc because the extracted features in a ```.mat``` file has already been provided. 

