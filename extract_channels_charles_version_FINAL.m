% Charles Hoffmeister
% UTSA Summer Internship Homework 1 

% Load the EEG Data file
load('EEG_driving_data_sample.mat');




%concat both datastructures into 1, 3 = over the third demension 
all_data = cat(3,data_class_0,data_class_1);
extracted_features = zeros(size(all_data,3),size(all_data,1)*2);
for i = 1:size(all_data,3) %Looping over Epochs
    %30 channels X 250 Samples for ith epoch
    data=all_data(:,:,i);
   
    
    %Theta Channel extraction / 250Hz sampling rate as stated in problem    
    %Transpose data so 30 channels along the columns instea of rows
    % to meet bandpower reuirements 
    % Extracted represents the Theta power for each of the 30 electrodes
    % that were on the head 
    extracted = [bandpower(data', 250, [4 8]), bandpower(data', 250, [8 13])];
    extracted_features(i,:) = extracted;
    
end

save('exatracted_features.mat',"extracted_features");
